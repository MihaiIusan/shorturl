package com.mihai.shorturl.service;

/**
 * Service used to shorten an URL by creating a key
 *
 * @author Mihai Iusan
 */
public interface KeyService {
    String shorten(String url);
}