package com.mihai.shorturl.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.URI_TOO_LONG)
public class UriTooLongException extends UrlException {

    public UriTooLongException(String message) {
        super(message);
    }

    public UriTooLongException(String message, Throwable cause) {
        super(message, cause);
    }
}