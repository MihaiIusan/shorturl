package com.mihai.shorturl.service.verifier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * This class is used to verify URLs before they are saved to the database
 *
 * @author Mihai Iusan
 */
@Component
public class UrlVerifiers {
    private final List<UrlVerifier> verifiers;

    @Autowired
    public UrlVerifiers(List<UrlVerifier> verifiers) {
        this.verifiers = verifiers;
    }

    /**
     * Check if the URL is safe based on the verifiers
     *
     * @param url the URL to check
     * @return true if it passes the verifiers validation
     */
    public boolean isSafe(final String url) {
        boolean safe = true;

        for (UrlVerifier verifier : verifiers) {
            if (verifier != null) {
                boolean isValidByProvider = verifier.isSafe(url);
                if (!isValidByProvider) {
                    safe = false;
                    break;
                }
            }
        }

        return safe;
    }
}
