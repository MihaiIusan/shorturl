package com.mihai.shorturl.service.verifier;

/**
 * Interface for URL Verifiers used to check if a URL is valid
 */
public interface UrlVerifier {
    public boolean isSafe(final String url);
}
