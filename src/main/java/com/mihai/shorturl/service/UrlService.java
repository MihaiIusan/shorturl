package com.mihai.shorturl.service;

import com.mihai.shorturl.entity.UrlEntity;

import java.util.List;

/**
 * This is the URL service which is responsible to create save and find entities from the UrlRepository
 *
 * @author Mihai Iusan
 */
public interface UrlService {
    List<UrlEntity> find(int offset, int limit);

    UrlEntity find(String shortUrl);

    UrlEntity create(String url);

    String findUrlByKey(String key);

    void deleteByKey(String key);
}