package com.mihai.shorturl.service.impl;

import com.mihai.shorturl.service.KeyService;
import com.mihai.shorturl.service.exception.InvalidURLException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * This is a KeyService which retrieves a shortened key represented by a Base36 hash
 *
 * @author Mihai Iusan
 */
@Service
public class Base36HashService implements KeyService {
    private static final int RADIX = 36;
    private static final String PIPE = "-";

    /**
     * Gets the shortened representation of the URL
     *
     * @param url the long URL
     * @return the shortened value
     */
    @Override
    public String shorten(String url) {
        return encode(url);
    }

    /**
     * Encodes the URL to base 36 hash
     *
     * @param url supplied URL
     * @return the encoded URL
     */
    private String encode(String url) {
        if (StringUtils.isEmpty(url)) {
            throw new InvalidURLException("Supplied invalid url: empty");
        }

        String hexValue = Integer.toString(url.hashCode(), RADIX);
        if (hexValue.startsWith(PIPE)) {
            hexValue = hexValue.substring(1);
        }

        return hexValue;
    }

}