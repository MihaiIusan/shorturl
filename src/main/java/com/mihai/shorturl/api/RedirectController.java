package com.mihai.shorturl.api;

import com.mihai.shorturl.service.UrlService;
import com.mihai.shorturl.service.exception.UrlNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * This is a simple redirect using the short url key
 */
@Component
@Controller
@RequestMapping("/redirect")
public class RedirectController {
    private UrlService urlService;

    @Autowired
    public RedirectController(UrlService urlService) {
        this.urlService = urlService;
    }


    /**
     * Redirect to url by key
     * <p>
     * Note: At the moment it does not redirect to any page if it fails
     *
     * @param key the key
     * @return redirect to found URL
     */
    @GetMapping(value = "/{key}")
    public ModelAndView redirect(@PathVariable("key") String key) {
        final String url = urlService.findUrlByKey(key);
        if(url == null){
            throw new UrlNotFoundException("Could not find URL");
        }
        return new ModelAndView("redirect:" + url);
    }
}